
import { OrderPage } from '../order/order';
import { PacketPage } from '../packet/packet';
import { RiderPage } from '../rider/rider';
import { ClientPage } from '../client/client';

import 'rxjs/add/operator/map';


import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Toast } from '@ionic-native/toast';
import { DataServiceProvider } from '../../providers/data-service/data-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  checklists1 :any;
  products: any[] = [];
  selectedProduct: any;
  productFound:boolean = false;
  availableity :boolean  = false;
  constructor(public navCtrl: NavController,
    private barcodeScanner: BarcodeScanner,
    private toast: Toast,
    public dataService: DataServiceProvider) {
      this.dataService.getProducts()
      .subscribe((response)=> {
          this.products = response
          console.log(this.products);
      });
    this.checklists1 = [
      {
          title: 'https://images.pexels.com/photos/8204/night.jpg?auto=compress&cs=tinysrgb&h=350',
          action:''
          
      },
      {
        title: 'https://images.pexels.com/photos/8204/night.jpg?auto=compress&cs=tinysrgb&h=350'
        
      },
      {
        title: 'https://images.pexels.com/photos/8204/night.jpg?auto=compress&cs=tinysrgb&h=350'
       
      },
      {
        title: 'https://images.pexels.com/photos/8204/night.jpg?auto=compress&cs=tinysrgb&h=350'
       
      }

  ];
  }
  scan() {
    this.selectedProduct = {};
    this.barcodeScanner.scan().then((barcodeData) => {
      this.selectedProduct = this.products.find(product => product.plu === barcodeData.text);
      if(this.selectedProduct !== undefined) {
        this.productFound = true;
        console.log(this.selectedProduct);
      } else {
        this.selectedProduct = {};
        this.productFound = false;
        this.toast.show('Product not found', '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      }
    }, (err) => {
      this.toast.show(err, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });
  }


   
  Categary(availableity) {
console.log(availableity)
            }
  client(){
    this.navCtrl.setRoot(ClientPage);
  }
  packet(){
    this.navCtrl.setRoot(PacketPage);
  }
  rider(){
    this.navCtrl.setRoot(RiderPage);
  }
  order(){
    this.navCtrl.setRoot(OrderPage);
  }


  dogSelect() {
    // this.availableity = true
    }

  // catSelect() {
  //   console.log("play");
  //   }

  // turtleSelect() {
  //   console.log("pause");
    
   
  // }
}
