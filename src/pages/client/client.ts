import { Component, ViewChild, ElementRef } from '@angular/core';
import {  NavController, NavParams, Platform } from 'ionic-angular';
import { HomePage } from '../home/home';
// import { GoogleMaps, GoogleMap, GoogleMapsEvent, Marker, LatLng, HtmlInfoWindow
import { Geolocation } from '@ionic-native/geolocation';

import { Diagnostic } from '@ionic-native/diagnostic';

// } from '@ionic-native/google-maps';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  LatLng,
  HtmlInfoWindow,
  Polyline
 } from '@ionic-native/google-maps';
import { OrderPage } from '../order/order';
/**
 * Generated class for the ClientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@Component({
  selector: 'page-client',
  templateUrl: 'client.html',
})
export class ClientPage {
  map: GoogleMap;
  @ViewChild('map') element;
  @ViewChild('map')
  mapRef: ElementRef;
  @ViewChild('map') mapElement: ElementRef;
  start = 'chicago, il';
  end = 'chicago, il';
  directionsService:any;
  directionsDisplay:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public googleMaps: GoogleMaps,
    private platform: Platform,private diagnostic: Diagnostic,private geolocation: Geolocation
) {
  this.directionsService = new google.maps.DirectionsService;
  this.directionsDisplay = new google.maps.DirectionsRenderer;
  // this.location = new LatLng(42.346903, -71.135101);
  let successCallback = (isAvailable) => { console.log('Is available? ' + isAvailable); };
  let errorCallback = (e) => console.error(e);
  
  this.diagnostic.isLocationEnabled().then(successCallback).catch(errorCallback);
  
  // only android
  this.diagnostic.isGpsLocationEnabled().then(successCallback, errorCallback);
  }
  ngAfterViewInit() {
  }
  ionViewDidLoad() {
    this.platform.ready().then(() => {
      // this.initMap();
      this.loadMap()
    });
  
  }
  loadMap() {
    // this.geolocation.getCurrentPosition().then((resp) => {
    //   console.log(resp.coords.latitude)
    //   console.log(resp.coords.longitude)
     
    //   this.map = GoogleMaps.create('map_canvas');
      
    //           this.map.addMarker({
               
    //             position: {
    //               lat:   resp.coords.latitude,
    //               lng:  resp.coords.longitude
    //             },
    //             title: '@ionic-native/google-maps',
    //             icon: 'blue',
             
    //             zoom: 10,
    //           }).then((marker: Marker) => {
              
    //             marker.showInfoWindow();
              
    //           });

    //  }).catch((error) => {
    //    console.log('Error getting location', error);
    //  });
              

     let watch = this.geolocation.watchPosition();
     watch.subscribe((data) => {
      console.log(data.coords.latitude)
      console.log(data.coords.longitude)
      const location: LatLng = new LatLng(data.coords.latitude,data.coords.longitude);

      const location1: LatLng = new LatLng(24.9157095,67.0906278);
      const location2: LatLng = new LatLng(24.9418196,67.1185136);
      
      let AIR_PORTS = [
        location,  location1, location2
      ];
    
   
   

      const mapOptions: GoogleMapOptions = {
        camera: {
          target: location,
          zoom: 18
        },
        mapType: 'MAP_TYPE_TERRAIN'
      };
  
      this.map = GoogleMaps.create('map_canvas');
      this.map.addPolyline({
        points: AIR_PORTS,
        'color' : 'Black',
        'width': 10,
        'geodesic': true
    });


    
    
        this.map.addMarker({
          
           position: {
             lat:   data.coords.latitude,
             lng:  data.coords.longitude
           },
           title: '@ionic-native/google-maps',
           icon: 'blue',
        
         }).then((marker: Marker) => {
         
           marker.showInfoWindow();
         
         });
      });
      
            
  
      }

  onclose(){
    this.navCtrl.setRoot(HomePage);
  }
  onButtonClick(){
    this.navCtrl.setRoot(OrderPage)
  }
}
