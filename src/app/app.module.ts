import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Toast } from '@ionic-native/toast';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { ClientPage } from '../pages/client/client';
import { MapPage } from '../pages/map/map';
import { OrderPage } from '../pages/order/order';
import { PacketPage } from '../pages/packet/packet';
import { RiderPage } from '../pages/rider/rider';
import { TestPage } from '../pages/test/test';
import { DataServiceProvider } from '../providers/data-service/data-service';
import { HttpModule } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { GoogleMaps } from "@ionic-native/google-maps";
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SigninPage,
    SignUpPage,
    ClientPage,
    MapPage,
    OrderPage,
    PacketPage,
    RiderPage,
    TestPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CommonModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SigninPage,
    SignUpPage,
    ClientPage,
    MapPage,
    OrderPage,
    PacketPage,
    RiderPage,
    TestPage
  ],
  providers: [
    HttpClient,
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    Toast,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataServiceProvider,
    GoogleMaps,
    Geolocation,
    Diagnostic,
    File,
    Transfer,
    FilePath,
    FileTransfer,
  FileTransferObject,
  Camera,
    
  ]
})
export class AppModule {}
